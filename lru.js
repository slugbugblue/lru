/** Least Recently Used cache
 * @copyright 2023
 * @author Chad Transtrum <chad@transtrum.net>
 * @license Apache-2.0
 */

/** LRU cache: a class for remembering things. It uses a Map internally and
 * exposes most of the same interface.
 */
export class LRU {
  /** @type Map */
  #cache
  #capacity = 0
  #hits = 0
  #misses = 0
  #expired = 0

  /** Instantiate this class with the max number of cached items
   * @arg {number} capacity - the maximum number of items to cache
   */
  constructor(capacity = 1000) {
    this.#cache = new Map()
    if (typeof capacity === 'number' && capacity >= 1) {
      this.#capacity = Math.floor(capacity)
    } else {
      throw new SyntaxError('capacity must be a positive integer')
    }
  }

  /** The maximum capacity. */
  get capacity() {
    return this.#capacity
  }

  /** The current size. */
  get size() {
    return this.#cache.size
  }

  /** Number of times we have had a cache hit. */
  get hits() {
    return this.#hits
  }

  /** Number of times we have had a cache miss. */
  get misses() {
    return this.#misses
  }

  /** Number of times we have expired a cache entry. */
  get expired() {
    return this.#expired
  }

  /** Retrieve an item from the cache.
   * @arg {any} key - the key of the cached item
   * @returns {any} - the cached item or undefined
   */
  get(key) {
    if (this.#cache.has(key)) {
      // Update the least recently used order
      const value = this.#cache.get(key)
      this.#cache.delete(key)
      this.#cache.set(key, value)
      this.#hits++
    } else {
      this.#misses++
    }

    return this.#cache.get(key)
  }

  /** Retrieve an item from the cache without triggering any LRU handling.
   * @arg {any} key - the key of the cached item to retrieve
   * @returns {any} - the cached item or undefined
   */
  peek(key) {
    return this.#cache.get(key)
  }

  /** Store an item in the cache.
   * @arg {any} key - the key of the cached item
   * @arg {any} value - the value to store
   */
  set(key, value) {
    // To update an item, remove and re-add it to track LRU status
    this.#cache.delete(key)

    // If there are too many items, expire one before adding more
    if (this.#cache.size >= this.#capacity) {
      this.#expired++
      const [expire] = this.#cache[Symbol.iterator]().next().value
      this.#cache.delete(expire)
    }

    this.#cache.set(key, value)
  }

  /** Delete an item from the cache.
   * @arg {any} key - the key of the cached item
   * @returns {boolean} true if the item was in the cache
   */
  delete(key) {
    return this.#cache.delete(key)
  }

  /** Clear the cache and all its statistics. */
  clear() {
    this.#cache.clear()
    this.#hits = 0
    this.#misses = 0
    this.#expired = 0
  }

  /** Get an iterator to return [key, value] pairs from the cache. Does not
   * update any LRU information. */
  entries() {
    return this.#cache.entries()
  }

  /** Determine if a key is already in the cache. Does not trigger hit/miss.
   * @arg {any} key - the key to check
   * @returns {boolean} true if the key is in the cache
   */
  has(key) {
    return this.#cache.has(key)
  }

  /** Get an iterator for the keys stored in cache, in order from least to most
   * recent. Does not update any LRU information. */
  keys() {
    return this.#cache.keys()
  }

  /** Get an iterator for the values stored in cache, in order from least to
   * most recent. Does not update any LRU information. */
  values() {
    return this.#cache.values()
  }

  // Play nicely with the rest of the world
  toString() {
    return `LRU(${this.#cache.size} of ${this.#capacity})`
  }

  [Symbol.for('nodejs.util.inspect.custom')](_, options) {
    const [y, s, n] = [options.stylize, 'special', 'number']
    return `${y('LRU', s)}(${y(this.#cache.size, n)} of ${y(
      this.#capacity,
      n,
    )})`
  }
}
