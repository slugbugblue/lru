# @slugbugblue/lru

## 1.0.0 - 2023-03-16

- Pulled out of `@slugbugblue/trax` into its own repository for an initial
  independent release
